# Instructions

1. Download Fuseki2 from [here](https://jena.apache.org/download/index.cgi):
2. Unzip and open `run/shiro.ini` then comment line `#/$/** = localhostFilter` and uncomment line `/$/** = anon`
3. Start Fuseki2 (in command line `java -jar fuseki-server.jar`)
4. Open [this](http://localhost:3030/manage.html) page and add new dataset with name `libgen`
5. Download [this file](https://gitlab.com/LibraryGenesis/exodus-db/raw/master/libgen1000.ttl.zip) and upload it to Fuseki2 [here](http://localhost:3030/dataset.html)
6. Open [this page](http://librarygenesis.gitlab.io/exodus/)